class CreateCommands < ActiveRecord::Migration
  def change
    create_table :commands do |t|
      t.integer :post_id
      t.text :body

      t.timestamps
    end
  end
end

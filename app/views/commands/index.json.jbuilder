json.array!(@commands) do |command|
  json.extract! command, :id, :post_id, :body
  json.url command_url(command, format: :json)
end
